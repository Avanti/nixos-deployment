{
  description = "MuCCC Infrastructure Deployment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    muccc-api = {
      url = "git+https://gitlab.muc.ccc.de/muCCC/api";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    luftschleuse2 = {
      url = "github:muccc/luftschleuse2/luftschleuse3";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, nixos-hardware, muccc-api
            , luftschleuse2, home-manager, nixos-generators, ... }: let
    supportedSystems = flake-utils.lib.defaultSystems;
  in flake-utils.lib.eachSystem supportedSystems (system: let
    pkgs = import nixpkgs { inherit system; };
  in rec {
    defaultPackage = pkgs.colmena;
    devShell = pkgs.mkShell {
      packages = with pkgs; [ colmena ];
    };
  }) // {
    sdimage.aarch64-linux = nixos-generators.nixosGenerate {
      pkgs = import nixpkgs { system = "aarch64-linux"; };
      format = "sd-aarch64";
    };
  } // {
    colmena = {
      meta = {
        nixpkgs = import nixpkgs { };
        specialArgs.flakes = {
          inherit self nixpkgs nixos-hardware muccc-api luftschleuse2 home-manager;
        };
      };

      briafzentrum = { name, nodes, pkgs, ... }: {
        imports = [
          ./modules/default.nix
          "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
          ./briafzentrum.nix
        ];
      };

      nixbus = { name, nodes, pkgs, ... }: {
        deployment.targetHost = "${name}.club.muc.ccc.de";
        imports = [
          ./modules/default.nix
          "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
          ./nixbus.nix
        ];
      };

      loungepi = { name, nodes, pkgs, ... }: {
        deployment.targetHost = "192.168.178.36";
        deployment.allowLocalDeployment = true;
        nixpkgs.system = "aarch64-linux";
        imports = [
          ./modules/default.nix
          home-manager.nixosModules.home-manager
          ./loungepi.nix
        ];
      };

      luftschleuse = { name, nodes, pkgs, ... }: {
        deployment.targetHost = "luftschleuse.club.muc.ccc.de";
        deployment.allowLocalDeployment = true;
        nixpkgs.system = "aarch64-linux";
        imports = [
          ./modules/default.nix
          ./luftschleuse.nix
        ];
      };
    };
  };
}
