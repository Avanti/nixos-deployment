{ pkgs, lib, ... }:

with lib;

{
  boot.loader.timeout = mkForce 1;
  boot.loader.grub.splashImage = null;
  boot.loader.grub.device = mkForce "/dev/vda";
  boot.kernelParams = [ "console=ttyS0" "console=tty0" ];

  hardware.enableAllFirmware = mkForce false;

  fileSystems."/" = {
    device = mkForce "/dev/disk/by-label/nixos";
  };

  networking.hostName = "briafzentrum";
  networking.usePredictableInterfaceNames = false;
  networking.useDHCP = false;
  networking.interfaces.eth0.ipv4.addresses = [ { address = "83.133.178.166"; prefixLength = 28; } ];
  networking.defaultGateway = "83.133.178.161";
  networking.nameservers = [ "::1" "127.0.0.1" ];

  networking.firewall.allowedTCPPorts = [ 80 443 25 ];
  networking.firewall.logRefusedConnections = false;

  nix.maxJobs = 3;
  nix.buildCores = 2;

  users.extraUsers.root = {
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCs/VM56N9OsG/hK7LEwheHwptClBNPdBl/tIW8URWyQPsE0dN2FYAERsHom3I3IvAS3phfhYtLOwrQ+MqEt7u5f/E3CgdfvEFRER12arxlT/q3gSh5rUdq508fTjkUNmJr6Vul+BCZ7VeESa2yvvTesFqvdVP9NtpGbAusX/JCrXwQciygJ0hDuMdLFW8MmRzljDoBsyjz18MDaMzsGQddQuE+3uAzJ1NXZpNh+M+C6eLNe+QJQMb9VTPGB3Pc0cU0GWyXYpWTVkpJqJVe180ldMU9x2c2sBBcRM3N/UDn2MF3XQi3TdGO93AIcUHNCLmUvIdqz+DPdKzCt3c3HvHh fpletz@lolnovo"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK20Lv3TggAXcctelNGBxjcQeMB4AqGZ1tDCzY19xBUV fpletz@yolovo"
    ];
  };

  environment.systemPackages = with pkgs; [ mailutils ];

  services.kresd = {
    enable = true;
    listenPlain = [ "127.0.0.1:53" "[::1]:53" ];
  };

  services.redis.enable = true;

  services.nginx = {
    enable = true;
    virtualHosts."briafzentrum.muc.ccc.de" = {
      enableACME = true;
      forceSSL = true;
      root = "/nonexisting";
      basicAuth.briaftraeger = "briafgeheimnis";
      locations = {
        "/".extraConfig = ''
          allow ::1;
          allow 127.0.0.1;
        '';
        "/rspamd/" = {
          proxyPass = "http://[::1]:11334/";
        };
      };
    };
  };

  users.extraGroups.acme.members = [ "postfix" ];

  services.postfix = {
    enable = true;
    hostname = "briafzentrum.muc.ccc.de";
    destination = [ "briafzentrum.muc.ccc.de" ];
    relayDomains = [ "muc.ccc.de" "muenchen.ccc.de" "lists.muc.ccc.de" "badge.events.ccc.de" "r0ket.badge.events.ccc.de" "rad1o.badge.events.ccc.de" ];
    recipientDelimiter = "+";
    networks = [ "[::1]" "127.0.0.1" "194.126.158.124" "83.133.178.162" ];
    postmasterAlias = "fpletz@fnordicwalking.de";
    extraAliases = ''
      hostmaster: postmaster
      abuse: postmaster
      fpletz: fpletz@fnordicwalking.de
    '';
    transport = ''
      fpletz@muc.ccc.de local:
      muc.ccc.de smtp:zonk.muc.ccc.de:25
      briafzentrum.muc.ccc.de local:
      .muc.ccc.de smtp:zonk.muc.ccc.de:25
      muenchen.ccc.de smtp:zonk.muc.ccc.de:25
      badge.events.ccc.de smtp:zonk.muc.ccc.de:25
      .badge.events.ccc.de smtp:zonk.muc.ccc.de:25
    '';
    extraConfig = ''
      biff = no
      append_dot_mydomain = no
      delay_warning_time = 4h
      mailbox_size_limit = 0

      smtpd_recipient_restrictions =
        permit_mynetworks,
        permit_sasl_authenticated,
        permit_auth_destination,
        reject_unauth_destination,
        reject_unknown_recipient_domain,
        reject_unverified_recipient
      smtpd_relay_restrictions =
        permit_mynetworks,
        permit_sasl_authenticated,
        permit_auth_destination,
        reject_unauth_destination,
        reject_unknown_recipient_domain,
        reject_unverified_recipient
      #smtpd_sender_restrictions = reject_authenticated_sender_login_mismatch
      smtpd_helo_required = yes
      disable_vrfy_command = yes
      unverified_recipient_reject_reason = Address lookup failed
      smtpd_discard_ehlo_keywords = silent-discard, dsn

      smtpd_tls_auth_only = yes
      smtpd_tls_cert_file = /var/lib/acme/briafzentrum.muc.ccc.de/fullchain.pem
      smtpd_tls_key_file = /var/lib/acme/briafzentrum.muc.ccc.de/key.pem
      smtpd_tls_loglevel = 1
      smtpd_tls_received_header = yes
      smtpd_tls_security_level = may
      smtpd_tls_session_cache_database = btree:/var/lib/postfix/data/smtpd_scache
      smtpd_tls_session_cache_timeout = 3600s
      smtpd_tls_protocols = !SSLv2, !SSLv3
      smtpd_tls_ciphers = high
      smtpd_tls_exclude_ciphers = aNULL, MD5, RC4, 3DES, DES, PSK, DSS, SRP, ADH, AECDH
      smtpd_tls_CAfile = /etc/ssl/certs/ca-certificates.crt

      tls_ssl_options = no_ticket, no_compression
      tls_preempt_cipherlist = yes
      tls_append_default_CA = yes

      smtp_tls_security_level = may
      smtp_tls_loglevel = 1
      smtp_tls_protocols = !SSLv2, !SSLv3
      smtp_tls_ciphers = high
      smtp_tls_exclude_ciphers = aNULL, MD5, RC4, 3DES, DES, PSK, DSS, SRP, ADH, AECDH
      smtp_address_preference = ipv6
    '';
  };

  services.prometheus.exporters.postfix.enable = true;
  users.users.postfix-exporter.extraGroups = [ "systemd-journal" ];

  #services.mailman = {
  #  enable = false;
  #  siteOwner = "root@briafzentrum.muc.ccc.de";
  #  fromEmail = "mailman@briafzentrum.muc.ccc.de";
  #  hyperkittyApiKey = "";
  #  webSecretKey = "";
  #  restApiPassword = "";
  #  baseURL = "briafzentrum.muc.ccc.de";
  #  allowedArchivingHosts = [ "83.133.178.166" ];
  #};

  services.postfix.config = {
    smtpd_milters = [ "unix:/run/rspamd/rspamd.sock" ];
    milter_protocol = "6";
    milter_mail_macros = "i {mail_addr} {client_addr} {client_name} {auth_authen}";
    milter_default_action = "tempfail";
  };

  services.rspamd = {
    enable = true;
    workers.controller.bindSockets = [ "[::1]:11334"  {
      socket = "/run/rspamd/rspamd-worker.sock";
      mode = "0666";
      owner = "rspamd";
      group = "rspamd";
    }];
    workers.rspamd_proxy.bindSockets = [{
      socket = "/run/rspamd/rspamd.sock";
      mode = "0666";
      owner = "rspamd";
      group = "postfix";
    }];
    workers.rspamd_proxy.extraConfig = ''
      upstream "local" {
        self_scan = yes;
      }
    '';

    extraConfig = ''
      local_addrs = "127.0.0.1, ::1, 194.126.158.124, 83.133.178.162";

      .include(try=true,priority=1,duplicate=merge) "${pkgs.writeText "rspamd.conf.local" ''
        milter_headers {
          use = ["x-spamd-bar", "x-spam-level", "x-spam-status", "authentication-results", "x-spam-flag"];
          routines {
            x-spam-flag {
              header = "X-Spam-Flag";
              value = "YES";
            }
          }
        }

        settings {
          authenticated {
            priority = high;
            authenticated = yes;
            apply {
              groups_disabled = ["rbl", "spf"];
            }
          }
        }

        redis {
          servers = "127.0.0.1";
          db = 1;
        }

        classifier "bayes" {
          autolearn = [-2, 10];
        }

        mx_check {
          enabled = true;
        }

        history_redis {
          nrows = 1000;
        }

        surbl {
          enabled = false;
        }

       neural {
         enabled = false;
       }

        metric {
          name = "default";
          symbol "RCVD_VIA_SMTP_AUTH" {
            score = -0.15;
          }
          group "bayes" {
            symbol "BAYES_SPAM" {
              score = 7.0;
            }
            symbol "BAYES_HAM" {
              score = -7.0;
            }
          }
          group "excessqp" {
            symbol "FROM_EXCESS_QP" {
              score = 1.5;
            }
          }
          group "hfilter" {
            symbol "HFILTER_HOSTNAME_UNKNOWN" {
              score = 3.5;
            }
            symbol "HFILTER_FROMHOST_NORES_A_OR_MX" {
              score = 2.5;
            }
            symbol "HFILTER_FROMHOST_NORESOLVE_MX" {
              score = 1.5;
            }
            symbol "HFILTER_HELO_NORES_A_OR_MX" {
              score = 1.5;
            }
          }
          group "header" {
            symbol "RDNS_NONE" {
              score = 3.0;
            }
            symbol "FORGED_RECIPIENTS_MAILLIST" {
              score = -0.5;
            }
            symbol "FORGED_SENDER_MAILLIST" {
              score = -0.5;
            }
            symbol "MAILLIST" {
              score = -0.5;
            }
            symbol "SUBJECT_HAS_CURRENCY" {
              score = 2.5;
            }
            symbol "SUBJ_ALL_CAPS" {
              score = 3.0;
            }
            symbol "LONG_SUBJ" {
              score = 3.0;
            }
            symbol "SUBJECT_ENDS_EXCLAIM" {
              score = 0.7;
            }
            symbol "HAS_X_PRIO_THREE" {
              score = 1.5;
            }
            symbol "FORGED_SENDER" {
              score = 1.0;
            }
          }
          group "rbl" {
            symbol "RCVD_IN_DNSWL_HI" {
              score = -2.5;
            }
            symbol "RCVD_IN_DNSWL_MED" {
              score = -2.0;
            }
            symbol "RCVD_IN_DNSWL_LOW" {
              score = -1.5;
            }
            symbol "RCVD_IN_DNSWL_NONE" {
              score = -0.2;
            }
            symbol "RWL_MAILSPIKE_GOOD" {
              score = -2.0;
            }
            symbol "RWL_MAILSPIKE_VERYGOOD" {
              score = -2.5;
            }
            symbol "RWL_MAILSPIKE_EXCELLENT" {
              score = -3.5;
            }
            symbol "RWL_MAILSPIKE_POSSIBLE" {
              score = -1.0;
            }
            symbol "RECEIVED_SPAMHAUS_XBL" {
              score = 5.5;
            }
            symbol "RECEIVED_SPAMHAUS" {
              score = 3.0;
            }
            symbol "RBL_SPAMHAUS_CSS" {
              score = 2.5;
            }
            symbol "RBL_SPAMHAUS_XBL" {
              score = 2.5;
            }
            symbol "RBL_SENDERSCORE" {
              score = 2.5;
            }
          }
          group "compromised_host" {
            symbol "HAS_PHPMAILER_SIG" {
              score = 1.5;
            }
            symbol "MID_RHS_WWW" {
              score = 1.0;
            }
            symbol "HAS_X_POS" {
              scpre = 1.0;
            }
            symbol "X_PHP_EVAL" {
              score = 5.0;
            }
          }
          group "encryption" {
            symbol "RCVD_NO_TLS_LAST" {
              score = 0.2;
            }
          }
          group "upstream_spam_filters" {
            symbol "PRECEDENCE_BULK" {
              score = 1.0;
            }
          }
          group "check_mx" {
            symbol "MX_INVALID" {
              score = 1.0;
              description = "No connectable MX";
              one_shot = true;
            }
            symbol "MX_MISSING" {
              score = 2.0;
              description = "No MX record";
              one_shot = true;
            }
            symbol "MX_GOOD" {
              score = -0.5;
              description = "MX was ok";
              one_shot = true;
            }
          }
          group "dmarc" {
            symbol "DMARC_POLICY_REJECT" {
              score = 5.0;
            }
            symbol "DMARC_POLICY_ALLOW" {
              score = -0.7;
            }
          }
        }
      ''}"
    '';
  };
}
