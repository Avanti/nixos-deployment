{ config, pkgs, ... }:

{
  users = {
    mutableUsers = false;
    extraUsers = {

      root = {
        subUidRanges = [ { count = 65536; startUid = 100000; } ];
        subGidRanges = [ { count = 65536; startGid = 100000; } ];
      };

      argv = {
        uid = 1000;
        group = "users";
        home = "/home/argv";
        createHome = true;
        isNormalUser = true;
        description = "argv";
        extraGroups = [
          "wheel"
        ];
        shell = "${pkgs.zsh}/bin/zsh";
      };

      fpletz = {
        uid = 1001;
        group = "users";
        home = "/home/fpletz";
        createHome = true;
        isNormalUser = true;
        description = "fpletz";
        extraGroups = [
          "wheel"
        ];
        shell = "${pkgs.zsh}/bin/zsh";
      };

      alice = {
        uid = 1002;
        group = "users";
        home = "/home/alice";
        createHome = true;
        isNormalUser = true;
        description = "alice";
        extraGroups = [
          "wheel"
        ];
        shell = "${pkgs.zsh}/bin/zsh";
      };
    };

    users.root = {
      hashedPassword = "$6$7zZRFBRJu$g2speYth4OYO0cJMpG5mZA6ACAB9TZSnsu8h6Oe4sM/Am6doJAABzzPXNl5zDvONnyP9eR39JgKNjHblHgE3z0";
    };

    users.argv = {
      hashedPassword = "$6$8FeyDBiQpP$BgUBmSEWi1F/Fyg5dbE3o8/Ca8w5E5pImPWJLln3U300gwDcpmeYksQjS.4HnBCSwU6xWiktFRFFwTIoeyHzq.";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDdjwnVKIRqf4PP5GQLmqEy3FoVcN6DqPGJlLinCMaiA argv@sheld0r"
      ];
    };

    users.fpletz = {
      hashedPassword = "$6$yiNdYNjs7$oguasC/f8AUbXt0vEGwuhcx3rEja9r5yIs1lKb1md/3y//W1rX7cWqWInMmXaLFFIUNHt.G1rhzMleKJ6SYa30";
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCs/VM56N9OsG/hK7LEwheHwptClBNPdBl/tIW8URWyQPsE0dN2FYAERsHom3I3IvAS3phfhYtLOwrQ+MqEt7u5f/E3CgdfvEFRER12arxlT/q3gSh5rUdq508fTjkUNmJr6Vul+BCZ7VeESa2yvvTesFqvdVP9NtpGbAusX/JCrXwQciygJ0hDuMdLFW8MmRzljDoBsyjz18MDaMzsGQddQuE+3uAzJ1NXZpNh+M+C6eLNe+QJQMb9VTPGB3Pc0cU0GWyXYpWTVkpJqJVe180ldMU9x2c2sBBcRM3N/UDn2MF3XQi3TdGO93AIcUHNCLmUvIdqz+DPdKzCt3c3HvHh fpletz@lolnovo"
      ];
    };

    users.alice = {
      hashedPassword = "$6$x0H0hv4/D$sVU7UFZJB2IB2OfJZmA0VcRI3WqjxDrS4sfEfv1Pnf14lVkKIzqm147VQL8mPtPzRYaPqcHjGx20hQkDyQu8G1";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIEY+j1gTRzOZU2QyeTQ4zgrf0ihJtCpAXzTpvJWH4O/ alice@ai"
      ];
    };
  };
}
