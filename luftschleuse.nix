{ config, pkgs, lib, ... }:

let
  sshCommandUsers = [
    { user = "open";
      msg = "Unlocking back door";
      cmd = "unlock";
    }
    { user = "openfront";
      msg = "Unlocking front door";
      cmd = "unlockfront";
    }
    { user = "close";
      msg = "Locking back door";
      cmd = "lock";
    }
  ];

in
{
  boot = {
    kernelPackages = pkgs.linuxPackages;
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };
    initrd.availableKernelModules = [ "vc4" ];
    kernelParams = [ "console=tty0" ];
  };

  hardware.enableRedistributableFirmware = true;

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
    "/boot/firmware" = {
      device = "/dev/disk/by-label/FIRMWARE";
      fsType = "vfat";
    };
  };

  networking = {
    hostName = "luftschleuse";
    useNetworkd = true;
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0 = {
      ipv4.addresses = [ { address = "192.168.2.2"; prefixLength = 24; } ];
    };
    firewall = {
      allowedUDPPorts = [ 53 67 ];
      allowedTCPPorts = [ 22 80 ];
    };
  };

  systemd.network.links."30-eth0" = {
    matchConfig.PermanentMACAddress = "b8:27:eb:02:53:2c";
    linkConfig = {
      MACAddress = "c4:93:00:11:0d:3d";
      MACAddressPolicy = "none";
    };
  };
  systemd.network.networks."40-wlan0" = {
    linkConfig.RequiredForOnline = false;
  };
  systemd.network.networks."40-eth0" = {
    linkConfig.RequiredForOnline = true;
    dhcpV4Config.ClientIdentifier = "mac";
  };

  environment.systemPackages = with pkgs; [ lm_sensors ];

  services.fail2ban.enable = false;

  services.openssh.extraConfig = lib.concatMapStrings (t: ''
    Match User ${t.user}
      LogLevel ERROR
      DisableForwarding yes
      AuthorizedKeysFile /etc/ssh/authorized_keys.d/%u /var/lib/authorized_keys/authorized_keys
      ForceCommand ${pkgs.writeScript "${t.user}.sh" ''
        #!${pkgs.stdenv.shell}
        set -o errexit
        echo '${t.msg}...'
        echo '${t.cmd}' | ${pkgs.netcat}/bin/nc -w 0 -u 127.0.0.1 2323
        echo 'Command sent.'
      ''}
  '') sshCommandUsers;

  users = {
    mutableUsers = false;
    users.root.openssh.authorizedKeys.keys = [
      # FIXME
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJY+/tAXZFm9U+nJt0kKo6e/TrYiH7E49n0ktbuF5I6 fpletz@fpine"
      # derchris
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLpCPJqrNGzWTtFGAox18Jyk13xfvd0ZlMRfyI5maBxRF+aSS9prX6l8OrXc+CLPGXlpwodi5tniAE7pfIqPQ6UnZDFuN/JKyaJ5I7ppMOqdkCbvxAOc/kLSsNAYXpOb1G9neE74JAHCa/z1lUyHTu/b4eyFzAdA8YmIeoN8HInQDUMgQ0OR6rRGAbDhi6oBi3I1gttBlLOFXCuSi9+sxWbRmT6pmtwzcxjlTIBVachh90sSY9tNir9QuPx4lrqrNnZgBOOFAjqYXCfjSZ9+Hkv9NFxa+r4aQ7uaTnqebqekvgCF37c1MKNEeuVsHVbDKEcJ3m5XttjoC/iV2A16CV"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP3VMGqSKqxQYbCTc1QPwTc3m8eHPSMqwUgE4D1k9Z95"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICsalcfDzctz5VN+BTYciykEsEOv0TfGYHAvkiPivVNC"
      # schneider
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkQjXVf4bW5tqE9j3bh5buCVQeLVTJm3tpr0cvsPB5hxLjGhrCzD//7a4REwXp3QzWIzv2sKSl98IC6jOsD8zx1e8sHddgh3l+FltfLs+G7f1GOc5VjpKUlo+v5kZBdNyjbhzww/lQ7WKotvf/Kn1TpsuUdFjGfg384CfFKdr6/VuRybceLrzVMYcInM0mXYZViGvBn3Z66Oaaainc+DYN8v4cNpRbVj+IoCf+d03HReUbxqhl4t0Os9wTz9lC4uPXKzF+HVhixSOXVOd7qlWpHrZ2rK8JbOuCc2k8jAvOm5TfWIxhSUQAdlczETVTOhNvy1hyC67Al/sHvM9WHWJD"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDe/Udd8U/ZBgGRfGT6nMYEs1aZ4NYjl55QeIrpHg3fb9xFHhJ6bFZyM6BVTeYroyvYRv1rXQLSYv7uzE7xa9WIznkjwyZUBH0WavWMlFds3YBER2UvFzJqu7989g1EWFTeEJM0tegkWFFwKtonVmnTdWVeWTdDWyIUlSLgpss/wxzStf22AmCXPH9AbQ3uWKRBqzSKeHq4mt9rcPXrhAbuY6g8/1yMBd2S25zm7jWFeQCXMOetniw2ugCF5TsUJEVBSxB4MGvUotUyugbEOzgIqOoBmUZ62x5FmqIZD9vcinbFlCX/grDtEbMQ60Re5LBU/95TVLiHH49fMdTo0jXF"
    ];
    users.open = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys = {
        keys = config.users.users.root.openssh.authorizedKeys.keys;
      };
    };
    users.openfront = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys = {
        keys = config.users.users.root.openssh.authorizedKeys.keys;
      };
    };
    users.close = {
      isNormalUser = true;
      group = "users";
      openssh.authorizedKeys = {
        keys = config.users.users.root.openssh.authorizedKeys.keys;
      };
    };
  };

  systemd.services.dnsmasq = {
    after = [ "sys-subsystem-net-devices-wlan0.device" ];
    bindsTo = [ "sys-subsystem-net-devices-wlan0.device" ];
  };
  services.dnsmasq = {
    enable = true;
    resolveLocalQueries = false;
    extraConfig = ''
      bind-interfaces
      interface=wlan0
      no-resolv
      dhcp-range=192.168.2.10,192.168.2.200,30m
      no-ping
      dhcp-option=option:router
      dhcp-option=option:dns-server
    '';
  };

  services.hostapd = {
    enable = true;
    interface = "wlan0";
    countryCode = "DE";
    ssid = "luftschleuse3";
    channel = 7;
    wpa = false;
  };

  services.nginx = {
    # FIXME: make android believe there is internetz
    enable = false;
    virtualHosts."_" = {
      root = "/nonexisting";
      locations."/generate_204".extraConfig = "return 204;";
    };
  };

  environment.etc."lockd.cfg".text = ''
    [Front Door]
    type = door
    address = A
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = False
    sequence_number_container_file = /tmp/front_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Back Door]
    type = door
    address = B
    key = 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    inital_unlock = True
    sequence_number_container_file = /tmp/back_door_rx_seq.log
    rx_sequence_leap = 32768
    timeout = 2

    [Master Controller]
    serialdevice = /dev/null
    baudrate = 115200

    [Master Controller Buttons]
    down = 2
    closed = 1
    member = 4
    public = 8

    [Master Controller LEDs]
    down = 0
    closed = 1
    member = 2

    [Logging]
    host = 127.0.0.1
    port = 23514
    level = debug

    [Display]
    display_type = Nokia_1600
    max_update_rate = .5

    [Status Receiver]
    host = 127.0.0.1
    port = 2080
  '';

  systemd.services.lockd = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ hostapd ];
    serviceConfig = {
      Restart = "always";
      ExecStart = "${pkgs.luftschleuse2-lockd}/bin/lockd /root/lockd.cfg";
    };
  };

  systemd.services.update-authorized-keys = {
    script = ''
      ${pkgs.curl}/bin/curl -s -o "/tmp/authorized_keys" \
        --header "PRIVATE-TOKEN: $(cat /root/gitlab-deploy-token)" \
        "https://gitlab.muc.ccc.de/api/v4/projects/169/jobs/artifacts/master/raw/authorized_keys?job=authorized_keys"
      [[ $(sha512sum < /tmp/authorized_keys) != $(sha512sum < $STATE_DIRECTORY/authorized_keys) ]] \
        && cp -v /tmp/authorized_keys $STATE_DIRECTORY/authorized_keys
      exit 0
    '';
    serviceConfig = {
      Type = "oneshot";
      StateDirectory = "authorized_keys";
    };
  };

  systemd.timers.update-authorized-keys = {
    wantedBy = [ "timers.target" ];
    timerConfig.OnCalendar = "*:0/5";
  };

  systemd.services.hostapd = {
    serviceConfig = {
      Restart = "always";
      RestartSec = "2s";
    };
  };

  services.prometheus.exporters = {
    node = {
      enable = true;
      enabledCollectors = [ "systemd" ];
      openFirewall = true;
      firewallFilter = "-i eth0 -p tcp --dport 9100";
      port = 9100;
    };
  };
}
